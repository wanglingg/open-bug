package com.cloud.bug.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_bug_attach")
public class BugAttach {

	private String id;
	private String bugId;
	
	private String fileName;
	private String extendType;
	private int fileSize;
	
	@Id
	@Column(unique = true, nullable = false)
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@Column(length = 36)
	public String getBugId() {
		return bugId;
	}
	
	public void setBugId(String bugId) {
		this.bugId = bugId;
	}
	
	@Column(length = 255)
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column(length = 10)
	public String getExtendType() {
		return extendType;
	}
	
	public void setExtendType(String extendType) {
		this.extendType = extendType;
	}
	
	@Column(length = 5)
	public int getFileSize() {
		return fileSize;
	}
	
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}
}
