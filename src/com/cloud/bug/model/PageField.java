package com.cloud.bug.model;

public class PageField {

	private String name;
	private String label;
	private String htmlType;
	private boolean required = false;
	
	public PageField(String name, String label, String htmlType) {
		this.name = name;
		this.label = label;
		this.htmlType = htmlType;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getHtmlType() {
		return htmlType;
	}
	
	public void setHtmlType(String htmlType) {
		this.htmlType = htmlType;
	}
	
	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
}
