function checkForm() {
	var check = true;
	$("div.validate").hide();
	
	// check require
	$(".input-require").each(function() {
		if($(this).val().trim() == "") {
			$(this).focus().parent().append("<div class='outline validate'>不能为空</div>");
			check = false;
			return false;
		}
	});
	
	return check;
}