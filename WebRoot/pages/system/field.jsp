<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<style>
		.input-text {width: 400px;}
	</style>
</head>

<body>
	<div class="wrapper">
	
		<div class="tabbable tabs-left">
			<ul class="nav nav-tabs">
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/system.jsp" />"><a href="#tab1" data-toggle="tab">修改密码</a></li>
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/systemName.jsp" />"><a href="#tab2" data-toggle="tab">系统名称</a></li>
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/bugCode.jsp" />"><a href="#tab3" data-toggle="tab">编号规则</a></li>
				<li class="active"><a href="#tab4" data-toggle="tab">字段管理</a></li>
			</ul>
			
			<div class="tab-content">
				<div id="tab4" class="tab-pane active">
					<div id="btnDiv">
						<button class="btn" onclick="modifyPwd();">添加字段</button>
					</div>
					
					<div id="listDiv">
						<table class="list-table">
							<tr>
								<th width="60px">序号</th>
								<th width="100px">类型</th>
								<th width="200px">字段名</th>
								<th width="200px">显示名</th>
								<th width="100px">页面类型</th>
							</tr>
						</table>
					</div>
				</div>
				
				<div id="tab1" class="tab-pane"></div>
				<div id="tab2" class="tab-pane"></div>
				<div id="tab3" class="tab-pane"></div>
			</div>
		</div>
		
	</div>
	
	<script>
		
	</script>
	
</body>
</html>
