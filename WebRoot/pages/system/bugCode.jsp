<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<style>
		.input-text {width: 400px;}
	</style>
</head>

<body>
	<div class="wrapper">
	
		<div class="tabbable tabs-left">
			<ul class="nav nav-tabs">
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/system.jsp" />"><a href="#tab1" data-toggle="tab">修改密码</a></li>
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/systemName.jsp" />"><a href="#tab2" data-toggle="tab">系统名称</a></li>
				<li class="active"><a href="#tab3" data-toggle="tab">编号规则</a></li>
				<li onclick="leftMenu($(this));" url="<c:url value="/pages/system/field.jsp" />"><a href="#tab4" data-toggle="tab">字段管理</a></li>
			</ul>
			
			<div class="tab-content">
				<div id="tab3" class="tab-pane active">
					<div id="btnDiv">
						<button class="btn" onclick="alert('正在努力地研发中...');">自定义编号规则</button>
					</div>
					
					<div class="title">默认缺陷编号规则</div>
					
					<div>
						《固定字符》"BUG" + 《系统自增序号》   
					</div>
				</div>
				
				<div id="tab1" class="tab-pane"></div>
				<div id="tab2" class="tab-pane"></div>
				<div id="tab4" class="tab-pane"></div>
			</div>
		</div>
	</div>
	
	<script>
	</script>
</body>
</html>
