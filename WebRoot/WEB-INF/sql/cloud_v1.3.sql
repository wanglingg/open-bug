CREATE TABLE t_bug_share (
    Id varchar(36) NOT NULL default '',
    bugId varchar(36) default NULL,
    createTime varchar(36) default NULL,
    PRIMARY KEY  (Id)
);

CREATE TABLE t_bug_field (
    Id varchar(36) NOT NULL default '',
    type int default 1,
    name varchar(255) default NULL,
    label varchar(255) default NULL,
    htmlType varchar(20) default NULL,
    PRIMARY KEY  (Id)
);

insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000000', 0, 'name', '缺陷名称', 'TEXT');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000001', 0, 'projectId', '所属项目', 'PROJECT');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000002', 0, 'auditorId', '审核人', 'USER');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000003', 0, 'intro', '缺陷描述', 'TEXTAREA');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000004', 0, 'reappear', '重现步骤', 'TEXTAREA');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000005', 0, 'note', '备注', 'TEXTAREA');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000006', 0, 'modifierId', '修改人', 'USER');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000007', 0, 'testorId', '测试人', 'USER');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000008', 0, 'solveInfo', '解决方法', 'TEXTAREA');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000009', 0, 'relateTest', '关联测试', 'TEXTAREA');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000010', 0, 'attach', '附件', 'ATTACH');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000011', 0, 'level', '严重性', 'SELECT');
insert into t_bug_field(Id, type, name, label, htmlType, isRequire) values('000000000000000000000000000000000012', 0, 'priority', '优先级', 'SELECT');